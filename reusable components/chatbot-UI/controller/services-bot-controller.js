const {
  newSession,
  getMessage,
  getMessageNoSession
} = require('../library/watson-assistant-service');

exports.createSession = ((req,res,next)=>{
  return newSession()
  .then(responce=> {
    process.env.SESSION_ID = responce.result.session_id;
    res.send(responce.result);
  })
  .catch(err => {
    next(err)
  });
})

exports.ask = ((req,res,next)=>{
    const session_id = req.get('session_id')
    // let context = null
    // if(process.env.CONTEXT){
    //   context = process.env.CONTEXT;
    // }
    // console.log('context : ',context);
    return getMessage(session_id,req.body)
    .then( responce=> {
        if(responce.status!==200)
          throw({message:responce.message,statusCode:responce.status})
        let resp_len = responce.result.output.generic.length;
        // process.env.CONTEXT = JSON.stringify(responce.result.context);
        let output = [];
        if(resp_len){
           output = responce.result.output.generic[resp_len-1]
        }
        res.send(output);
    })
    .catch(err => {
      next(err)
    });
})