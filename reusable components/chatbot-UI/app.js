require('dotenv').config();
const express = require('express')
const bodyParser = require("body-parser")
const servicesBot = require('./routes/services-bot-route')
const app = express()

app.use(express.static('views'))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

app.use((req,res,next)=>{
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods','GET,POST');
    res.setHeader('Access-Control-Allow-Headers','Content-Type,Authorization');
    next();
})

app.use('/service',servicesBot);

app.use((error,req,res,next)=>{
    console.log(error);
    const status = error.statusCode || 404;
    const message = error.message || 'Bad Request';
    res.status(status).json({message:message});
})

module.exports = app;