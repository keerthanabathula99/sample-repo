const AssistantV2 = require('ibm-watson/assistant/v2');
const { IamAuthenticator } = require('ibm-watson/auth');

const assistant = new AssistantV2({
  version: process.env.WATSON_VERSION,
  authenticator: new IamAuthenticator({
    apikey: process.env.WATSON_API_KEY
  }),
  serviceUrl:  process.env.WATSON_URL
});

const newSession = () =>
  assistant
    .createSession({
      assistantId: process.env.WATSON_ASSISTANT_ID
    })
    .then(response => {
      console.log("session_id --> : ",JSON.stringify(response.result, null, 2));
      return response;
    })
    .catch(err => {
      console.log(err);
      return err;
    });


const getMessage = (session_id,body) =>
  assistant
    .message({
      assistantId:  process.env.WATSON_ASSISTANT_ID,
      sessionId: session_id,// process.env.SESSION_ID,
      input: {
        'message_type': 'text',
        'text': body.input ,
        'options': {
          'return_context': true
        }
      },
      user_id:"wavelabstestid123"
      // context:context
    })
    .then(response => {
      console.log(JSON.stringify(response.result, null, 2));
      if(response.result.output.generic.length)
        return response
      else
        return getMessage(session_id,body)
    })
    .catch(err => {
      console.log(err);
      return err;
    });

const getMessageNoSession = (body) =>
    assistant
      .messageStateless({
        assistantId:  process.env.WATSON_ASSISTANT_ID,
        input: {
          'message_type': 'text',
          'text': body.input ,
        }
      })
      .then(response => {
        console.log(response);
        return response
      })
      .catch(err => {
        console.log(err);
        return err;
      });

module.exports = {
  newSession,
  getMessage,
  getMessageNoSession
}
  


