const app = require('./app');
const http = require("http").Server(app)
const io = require("socket.io")(http)

const port = process.env.PORT || 3000

io.on('connection',()=>{
    console.log('User connected');
})
const server = http.listen(port, () => console.log('Listening on port ', server.address().port))