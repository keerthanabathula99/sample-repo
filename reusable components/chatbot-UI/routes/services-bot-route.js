const express = require("express");
const router = express.Router();
const {createSession,ask} = require('../controller/services-bot-controller');

router.get('/createsession', createSession)
router.post('/ask', ask)

module.exports=router;

