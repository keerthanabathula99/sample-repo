# Reusable components in IBM Watson

There are the four Reusable components which you can use in any chatbot of IBM Watson
1. Greet Intent
2. Stop Intent
3. Help Intent
4. anything_esle

## Greet Intent

greet intent is used in the chatbot when user opens the chatbot we can use the greet intent to greet the user

### Stop Intent

stop intent is used in the chatbot where user want to end the conversation when the user enters stop the conversation gets stop.

### Help Intent

Help intent is used in the chatbot when user need any help.

#### anything_esle
anything_esle is used in the chatbot when the user enters something which is not trained by the bot.

##### process of using reusable components

# Adding the dialog flow 

- Upload the  Dialogue.Json file which is present in Reusable components folder

![upload_dialog_file](/uploads/c38a4c8ee80de3758268274ee7e66816/upload_dialog_file.png)

![intents_you_get](/uploads/99f1bdb89893cf5266187d9f75e29110/intents_you_get.png)

![dialog](/uploads/311c22f02739cc1952a4e69dade54e12/dialog.png)

# Adding extension
- For adding extentions go to intergrations

![go_to_integrations](/uploads/1730045ad67fc66b66eafda93e230eb3/go_to_integrations.png)

- Now, click on the build extention

![click_on_build_extension](/uploads/177b13bffbf242084386fd8382ee993b/click_on_build_extension.png)

- You have to mention the extension name, description and click on next

![give_name_and_description_and_click_next](/uploads/3e98c8663e67bd302e87fa9fe76fc643/give_name_and_description_and_click_next.png)

- upload the  swagger-documentation.Json file present in Reusable components folder

![import_swagger_file_and_click_next](/uploads/8ca3c61ebf808c4a6a829fb83384eac6/import_swagger_file_and_click_next.png)

# Adding actions:
 - Upload the Action.Json file which is present in reusable compenents folder

![in_action_go_to_dialog_setting](/uploads/58b5473e576ece6c3cb6a1ff28d0523a/in_action_go_to_dialog_setting.png)

![upload_actions_file](/uploads/7ab7a76cd156c3a1e606d816b989f7eb/upload_actions_file.png)

![now_you_will_get_the_action](/uploads/9745fb39ba49c1b4c897d3488e0276b1/now_you_will_get_the_action.png)

- Now, click on edit extention

![click_on_edit_extension](/uploads/5faf4fb7044dae65d479d1a8e50e623e/click_on_edit_extension.png)

![after_click_on_edit_extension_setup_this](/uploads/0dc39693d11d305bd4ef863411261fb7/after_click_on_edit_extension_setup_this.png)

## Setting up frontend for chatbot 

1: Extract the code from chatbot-UI.zip folder 

2: Open the file .env and change the WATSON_API_KEY, WATSON_URL, WATSON_ASSISTANT_ID with the one from step 3 

3: Open the file index.js from views folder and replace the http://localhost:3000 with domain name on which you will be hosting this code and save the file. 

4: Upload the code on server (based on the server follow the instruction to deploy node.js application). You can access the chatbot using domain name.  

## Reference video for how to create a sample chatbot

https://www.youtube.com/watch?v=pOZAPjcjgHo&t=213s

## Reference documentation for how to create a sample chatbot

https://cloud.ibm.com/docs/watson-assistant?topic=watson-assistant-welcome-new-assistant

## summary

This Readme.md file contains sample code of reusable compoments in the IBM Watson.









